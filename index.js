'use strict';

const puppeteer = require('puppeteer');
const nodemailer = require('nodemailer');
const yaml = require('js-yaml');
const fs = require('fs');
const dotenv = require('dotenv').config({ path: __dirname + '/.env' });
const locallydb = require('locallydb');

// Config var.
let config = {}

// Get default config from yaml.
if (fs.existsSync(__dirname + '/config.default.yml')) {
  config = yaml.load(fs.readFileSync(__dirname + '/config.default.yml', 'utf8'));
}
else {
  console.log('No default config file found.');
  return false;
}

// Get custom config from yaml.
if (fs.existsSync(__dirname + '/config.yml')) {
  const override_config = yaml.load(fs.readFileSync(__dirname + '/config.yml', 'utf8'));
  config = {...config, ...override_config};
}

// Create a screenshots dir if debug is enabled.
if (config.debug.enabled) {
  if (!fs.existsSync(__dirname + '/' + config.debug.screenshots_dir)) {
    fs.mkdirSync(__dirname + '/' + config.debug.screenshots_dir);
  }
}

// Setup mail transporter.
let transporter = null;
try {
  transporter = nodemailer.createTransport({
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    auth: {
      user: process.env.MAIL_USER,
      pass: process.env.MAIL_PASS
    }
  });
}
catch (e) {
  console_log_date('Could not set up nodemailer.');
  console.log(e);
  return;
}

// Get products from yaml.
let products = null;
if (fs.existsSync(__dirname + '/products.yml')) {
  products = yaml.load(fs.readFileSync(__dirname + '/products.yml', 'utf8'));
}
else {
  console_log_date('Could not read products file.');
  return;
}

// load the database (folder) in './pacdb', will be created if doesn't exist.
const db = new locallydb('./pacdb');

// load the collection (file) in './pacdb/products', will be created if doesn't
// exist.
const collection = db.collection('products');

// Contains the proxies.
let proxies = {};
// Proxy cid.
let proxy_cid = null;
// Proxy database.
let proxydb = null;

// Check if proxy is enabled.
if (config.proxy.enabled) {
  // Check if the source is database.
  if (config.proxy.source == 'db') {
    // load the collection in './pacdb/proxies', will be created if doesn't
    // exist.
    proxydb = db.collection('proxies');
    // Get a list of proxies.
    const proxies_from_db = proxydb.where({ available: 1 });
    proxies = proxies_from_db.items;
  }
  // Check if the source is list.
  else if (config.proxy.source == 'list' && config.proxy.hasOwnProperty('list')) {
    proxies = config.proxy.list;
  }
}

// Start puppeteer.
(async () => {
  // Start message.
  console_log_date('Starting Product Availability Checker...');

  // Object for browser arguments.
  const browser_args = {};

  // Check if use_proxy is true and there are proxies.
  if (config.proxy.enabled) {
    if (proxies.length > 0) {
      // Get a random number between 0 and the total nr of proxies.
      const random_nr = Math.floor(Math.random() * (proxies.length - 1));

      // Check if a proxy can be found with the random number.
      if (typeof proxies[random_nr] !== 'undefined') {
        // Save the cid for later.
        proxy_cid = proxies[random_nr].cid;
        // Compose a proxy address.
        const proxy = proxies[random_nr].protocol + '://' + proxies[random_nr].ip + ':' + proxies[random_nr].port;
        // Put the proxy as setting in the browser arguments.
        browser_args.args = [
          '--proxy-server=' + proxy
        ];
        // Let the user know we're using a proxy.
        console_log_date('Using proxy ' + proxy + ', ' + proxies.length + ' available.');
      }
    }
    else {
      console_log_date('Config is set to use proxy, but no proxies are available.');
    }
  }

  // Open browser.
  let browser = await puppeteer.launch(browser_args).catch((e) => {
    return null;
  });

  // On Raspberry Pi.
  if (!browser) {
    browser_args.executablePath = '/usr/bin/chromium-browser';
    browser = await puppeteer.launch(browser_args).catch((e) => {
      return null;
    });
  }

  // Stop if no browser is found.
  if (!browser) {
    console_log_date('Could not launch browser.');
    return false;
  }

  // Debug proxy.
  if (config.proxy.enabled && config.debug.enabled) {
    // Open a new page/tab.
    const page = await browser.newPage();
    // Get our ip from this website.
    await page.goto('https://httpbin.org/ip').catch((e) => {
      console_log_date('Could not connect to the ip checker.');
    });
    // Save it to a screenshot.
    await page.screenshot({ path: __dirname + '/' + config.debug.screenshots_dir + '/proxy_ip.png'});
    // Close the page.
    page.close();
    // Inform the user.
    console_log_date('Created screenshot proxy_ip.png.');
  }

  // Iterate through products.
  for (let i in products) {
    // Try to load the product from the database.
    let product_in_db = collection.where({ id: i });

    // Add it if it doesn't exists.
    if (!product_in_db.items[0]) {
      collection.insert({ id: i, status: '' });
      product_in_db = collection.where({ id: i });
    }

    // Open new page.
    const page = await browser.newPage();

    // Set the time-out to 60 seconds.
    await page.setDefaultNavigationTimeout(config.browser.navigation_timeout);

    // Set user agent (override the default headless User Agent).
    await page.setUserAgent(config.browser.agent);

    // Intercept the request.
    await page.setRequestInterception(true);
    // Block images to speed up things.
    page.on('request', (request) => {
      if (request.resourceType() === 'image') request.abort();
      else request.continue();
    });

    // Get the page status.
    let page_status = null;
    page.on('response', response => {
      if (response.url() == products[i].url) {
        page_status = response.status();
      }
    });

    // Go to the url and wait untill the page is fully loaded.
    let page_loaded = await page.goto(products[i].url, {
      waitUntil: 'networkidle2',
    }).then(() => {
      return true;
    }).catch((e) => {
      console_log_date('Error connecting to the product page (probably time-out).');
      // Set the availability of the used proxy to false to make sure we don't
      // use it again.
      if (config.proxy.enabled && config.proxy.disable_unavailable && proxy_cid != null) {
        proxydb.update(proxy_cid, { available: 0 });
        // Inform the user.
        console_log_date('We\'re not using this proxy again.');
      }

      return false;
    });

    // Check if the page status is 200.
    if (page_status !== 200) {
      console_log_date('The page status is not 200, instead we got ' + page_status + '.');

      // Set the availability of the used proxy to false to make sure we don't
      // use it again.
      if (config.proxy.enabled && config.proxy.disable_unavailable && proxy_cid != null) {
        proxydb.update(proxy_cid, { available: 0 });
        // Inform the user.
        console_log_date('We\'re not using this proxy again.');
      }

      page_loaded = false;
    }

    // Create a screenshot if debug in enabled.
    if (config.debug.enabled) {
      await page.screenshot({ path: __dirname + '/' + config.debug.screenshots_dir + '/' + i + '.png', fullPage: true });
      console_log_date('Created screenshot ' + i + '.png');
    }

    // Stop if the page is not loaded.
    if (!page_loaded) {
      // Close the page.
      page.close().catch(() => false);
      // Inform user.
      console_log_date('Error loading product page, skipping this product.');
      // Go to the next product.
      continue;
    }

    // Set default var.
    var selector_value = 'Not available';

    // Check if the selector exists.
    const exists = await page.$eval(products[i].selector, () => true).catch(() => false);

    // If so, get the value.
    if (exists) {
      // Get the attribute if it's set.
      if (typeof products[i].attribute !== 'undefined') {
        selector_value = await page.evaluate('document.querySelector("' + products[i].selector + '").getAttribute("' + products[i].attribute + '")');
      }
      // Otherwise, get the node value.
      else {
        selector_value = await page.evaluate('document.querySelector("' + products[i].selector + '").firstChild.nodeValue');
      }

      // Remove leading and trailing whitespaces.
      selector_value = selector_value.trim();
    }

    // We don't need the website anymore so close the page.
    page.close().catch(() => false);

    // Output status.
    console_log_date(products[i].store + "/" + products[i].product + ' status: ' + selector_value);

    // The status has changed when compared to the database status so do
    // something.
    if (selector_value != product_in_db.items[0].status) {
      // Inform user.
      console_log_date('Status changed.');

      // Default status is 'gone'.
      let mail_status = 'gone';

      // Check if price watch is enabled.
      if (typeof products[i].watch_price !== 'undefined' && products[i].watch_price == true) {
        // Convert the value to a numeric price without symbols.
        let price = products[i].value;
        price = price.toString().replace(/[^0-9\.\,]/g, '');

        // The price has dropped.
        if (selector_value < price) {
          mail_status = 'lowered in price - ' + selector_value;
        }
        // The price has increased.
        else {
          mail_status = 'increased in price - ' + selector_value;
        }
      }
      // The product is available.
      else if (selector_value == products[i].value) {
        mail_status = 'available';
      }

      // Send mail if the db status is not empty.
      if (config.mail_enabled && product_in_db.items[0].status != '') {
        console_log_date('Trying to send mail...');
        send_mail(products[i], mail_status);
      }

      // Update status in db.
      collection.update(product_in_db.items[0].cid, { status: selector_value });
    }
  }

  // Close browser.
  await browser.close();

  // Cleanup db.
  const all_products_in_db = collection.items;
  // Iterate through all items in de database.
  for (let i in all_products_in_db) {
    // If the db entry does not exist in the products yaml, delete it.
    if (typeof all_products_in_db[i].id !== 'undefined' && typeof products[all_products_in_db[i].id] === 'undefined') {
      collection.remove(all_products_in_db[i].cid);
    }
  }
})();

// Helper function returning a human readable date.
function console_log_date(message) {
  const date_ob = new Date();
  const day = ('0' + date_ob.getDate()).slice(-2);
  const month = ('0' + (date_ob.getMonth() + 1)).slice(-2);
  const year = date_ob.getFullYear();
  const hours = date_ob.getHours();
  const minutes = ('0' + date_ob.getMinutes()).slice(-2);
  const seconds = ('0' + date_ob.getSeconds()).slice(-2);

  const date = day + '-' + month + '-' + year + ' ' + hours + ':' + minutes + ':' + seconds;

  console.log(date + ' ' + message);
}

// Helper function to send a product status mail.
function send_mail(product, status) {
  // Create message.
  const message = {
    from: process.env.MAIL_FROM,
    to: process.env.MAIL_TO.split(','),
    subject: product.store + '/' + product.product + ' is ' + status,
    text: "Store: " + product.store + "\nProduct: " + product.product + "\nStatus: " + status + "\n\n" + product.url
  }
  // Send mail.
  transporter.sendMail(message, (err, info) => {
    if (err) {
      console_log_date('Mail not sent.');
      console.log(err);
    }
    else {
      console_log_date('Mail successfully sent.');
    }
  });
}
