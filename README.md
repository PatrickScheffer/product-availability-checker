# Product Availability Checker
A NodeJS powered app checking the availability of products on webshops.

The app opens a product url in Chromium (headless) and extracts the value of a
given selector. If the value of the selector matches the value specified in the
yaml, an e-mail is sent to the configured e-mail addresses.

## Dependencies
 * NodeJS
   * puppeteer
   * nodemailer
   * dotenv
   * js-yaml
   * locallydb
 * SMTP credentials of an e-mail account

## Setup
1. Install NodeJS, NPM and run `npm ci`.
2. Create an .env file with the following variables:
   * MAIL_HOST
   * MAIL_PORT
   * MAIL_USER
   * MAIL_PASS
   * MAIL_FROM
   * MAIL_TO
3. Create products.yml which contains the products to the the availability for.
   See products-example.yml for an example or below for an explanation.
4. Test the app by running `node index.js`.
5. If the test gives the desired output, setup crontab to run `node index.js`
   in the desired interval.

## Config
To create a custom config, copy `config.default.yml` to `config.yml` and make
your changes in there. These options are available:

```
debug:
  enabled: true or false
  screenshots_dir: folder name, will be created if it doesn't exist
mail_enabled: true or false
browser:
  navigation_timeout: time in milliseconds to wait for a page to load
  agent: a string containing a browser agent
proxy:
  enabled: true or false
  disable_unavailable: true or false, only available is source is db, marks not working proxies as unavailable
  source: db or list
  list: only used when source is list, requires an array of custom defined proxies
    -
      protocol: something like HTTP
      ip: self explanatory
      port: port of the proxy
```

## Proxy
When repeatingly sending requests to the same website, it might put your IP
address on a blacklist preventing you and everyone on the same network to visit
that website again. Proxies can prevent that as they let you send your request
through another IP address.

By setting `use_proxy: true` in `config.yml`, the script will use a random
proxy from a predefined source. This source can be a database or a custom list.

If the script fails to connect to the proxy it will
mark it as unavailable and won't use it a second time.

### Database
There are websites offering proxies as a paid service, but there are some free
ones. Of course, they are a lot less reliable, but at least they're free.

Executing `node generate_proxy_list.js` will extract all proxy addresses from
https://scrapingant.com/free-proxies/ and put them in a database.

Set `source: db` under the proxy settings in `config.yml` to use this database.

Setting `disable_unavailable: true` will mark a proxy as unavailable in the
database if the connection fails so it won't be used a second time. This can
result in fewer available proxies than there actually are.

### List
If `source: list` is set, there must be a `list` containing an array of proxies
containing the values `protocol`, `ip` and `port`. The script will always
randomnly pick one off this list.

## Configure products
This is how a product should be added to the yaml file:

```
unique-machine-name:
  store: Name of the store
  product: Name of the product
  url: URL to the product
  selector: The CSS selector of the element to check the value given below on
  attribute: Optional, check the value given below in this attribute
  value: The value the selector or its attribute should contain to indicate the product is available
  watch_price: Set this to true if the value contains a numeric value
```
See `products.example.yml` for example products.

## Troubleshooting
### The selector doesn't give the expected response
Setting `enabled: true` under the debug settings in `config.yml` will instruct
the app to save a screenshot of each product page to a directory defined in
`screenshot_dir`. This allows you to check if the page is correctly retrieved
and rendered.

If proxy is enabled, it will also create a screenshot of a ip checking website
allowing you to check if the proxy is correctly configured.

### Error: Failed to launch the browser process puppeteer
When installing this app on a Raspberry Pi, Puppeteer might not detect the
downloaded Chromium package. In that case, install the browser manually with
`sudo apt-get install chromium-browser`. The app will automatically check if
a Chromium executable can be found in `/usr/bin/chromium-browser` when the
default can't be found.

Also, you can look here for more troubleshooting with Puppeteer issues:
https://github.com/puppeteer/puppeteer/blob/main/docs/troubleshooting.md
