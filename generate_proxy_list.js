'use strict';

const debug = false;

const puppeteer = require('puppeteer');
const nodemailer = require('nodemailer');
const yaml = require('js-yaml');
const fs = require('fs');
const dotenv = require('dotenv').config({ path: __dirname + '/.env' });
const locallydb = require('locallydb');

// load the database (folder) in './pacdb', will be created if doesn't exist.
const db = new locallydb('./pacdb');

// Create new database.
const proxydb = db.collection('proxies');

// Start puppeteer.
(async () => {
  console.log('Starting free proxy list update...');

  // Open browser.
  let browser = await puppeteer.launch().catch((e) => {
    console.log(e);
    return null;
  });

  // On Raspberry Pi.
  if (!browser) {
    browser = await puppeteer.launch({
      executablePath: '/usr/bin/chromium-browser'
    }).catch((e) => {
      console.log(e);
      return null;
    });
  }

  if (!browser) {
    console.log('Could not launch browser.');
    return;
  }

  // Open new page.
  const page = await browser.newPage();

  // Configure the navigation timeout
  await page.setDefaultNavigationTimeout(0);

  // Set user agent (override the default headless User Agent).
  await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');

  // Intercept the request.
  await page.setRequestInterception(true);
  // Block images to speed up things.
  page.on('request', (request) => {
    if (request.resourceType() === 'image') request.abort();
    else request.continue();
  });

  // Go to the url and wait untill the page is fully loaded.
  await page.goto('https://scrapingant.com/free-proxies/', {
    waitUntil: 'networkidle0',
  }).catch((e) => {
    console.log(e);
  });

  // Check if the selector exists.
  const exists = await page.$eval('#proxies-list-loader tr', () => true).catch(() => false);

  if (exists) {
    // Get all proxies from the db.
    const all_proxies_in_db = proxydb.items;
    // Create a set for the delete records.
    const proxies_to_delete = new Set;
    // Put all cids in the delete set.
    for (let i in all_proxies_in_db) {
      if (typeof all_proxies_in_db[i].cid !== 'undefined') {
        proxies_to_delete.add(all_proxies_in_db[i].cid);
      }
    }

    // Get the proxies from the website.
    const proxies_to_keep = await page.evaluate(() => {
      // There are in a table.
      const tr = document.querySelectorAll('#proxies-list-loader tr');
      const proxies = [];

      // Iterate through the rows.
      for (let i in tr) {
        const td = tr[i].cells;

        // Skip table headers and other rows.
        if (typeof td === 'undefined' || td.length < 3 || td[0].nodeName != 'TD') {
          continue;
        }

        // Add the ip, port and protocol to the array.
        proxies[i] = {
          ip: td[0].innerText,
          port: td[1].innerText,
          protocol: td[2].innerText
        };
      }

      return proxies;
    });

    // Check if there are more than 1 items. It's 1 because there's a null in
    // there for some reason.
    if (proxies_to_keep.length > 1) {
      // Iterate through the new proxies.
      for (let i in proxies_to_keep) {
        // Check if we have a valid object.
        if (proxies_to_keep[i] && proxies_to_keep[i].hasOwnProperty('ip')) {
          // Check if this proxy already exists in the database.
          const proxy_in_db = proxydb.where(proxies_to_keep[i]);

          // If so.
          if (proxy_in_db.items.length > 0) {
            // Remove it from the delete array.
            if (proxies_to_delete.has(proxy_in_db.items[0].cid)) {
              proxies_to_delete.delete(proxy_in_db.items[0].cid);
            }
          }
          // If not, insert it.
          else {
            proxydb.insert({
              ip: proxies_to_keep[i].ip,
              port: proxies_to_keep[i].port,
              protocol: proxies_to_keep[i].protocol,
              available: 1
            })
            console.log('Added ' + proxies_to_keep[i].ip);
          }
        }
      }

      // Remove all proxies remaining in the delete array.
      if (proxies_to_delete.size > 0) {
        for (let cid of proxies_to_delete.values()) {
          proxydb.remove(cid);
          console.log('Removed ' + cid);
        }
      }

      console.log('Done.');
    }
    else {
      console.log('No proxies found to download.');
    }
  }
  else {
    console.log('No proxies found to download.');
  }

  // Close browser.
  await browser.close();
})();
